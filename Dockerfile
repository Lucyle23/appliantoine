#on démarre une image de base
FROM python:latest

#on copie les fichiers de notre app
COPY basic-flask-app ./appantoine

#on change de répertoire de travail
WORKDIR appantoine

#installation de venv
RUN python3 -m venv venv

#activation de venv
RUN . venv/bin/activate

#installation de Flask
RUN pip install -U Flask

#on lance l'app au lancement du conteneur
CMD ("python", "routes.py")
